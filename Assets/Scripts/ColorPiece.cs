﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ColorPiece : MonoBehaviour
{
    //we create an enum to store all of our color types.
    public enum ColorType
    { 
        YELLOW,
        PURPLE,
        RED,
        BLUE,
        GREEN,
        PINK,
        ANY,
        COUNT
    };

    //this struct stores our color type and the sprite associated with that type.
    [System.Serializable]
    public struct ColorSprites
    {
        public ColorType color;
        public Sprite sprite;
    }

    //we make an array of tiles made from our ColorSprite struct.
    public ColorSprites[] colorSprites;

    private ColorType color;

    public ColorType Color
    {
        get { return color; }
        set { SetColor(value); }
    }

    public int NumColors
    {
        get { return colorSprites.Length; }
    }

    private SpriteRenderer sprite;

    //now we convert that array into a Dictionary for easier lookup.
    private Dictionary<ColorType, Sprite> colorSpriteDict;

    void Awake()
    {
        //we set sprite equal to this gameObject's sprite renderer,
        //which is on a child object called "piece"
        sprite = transform.Find("piece").GetComponent<SpriteRenderer>();
        //we set our dictionary to a new dictionary to make sure it is empty
        colorSpriteDict = new Dictionary<ColorType, Sprite>();

        //we loop through all the structs
        for (int i=0; i<colorSprites.Length; i++)
        {
            //if the dictionary doesn't contain the current key,
            if (!colorSpriteDict.ContainsKey(colorSprites[i].color))
            {
                //we add the key/value pair
                colorSpriteDict.Add(colorSprites[i].color, colorSprites[i].sprite);
            }
        }
    }

    //this sets the color of this tile
    public void SetColor(ColorType newColor)
    {
        color = newColor;
        
        //if the dictionary already has this color as a key
        if(colorSpriteDict.ContainsKey(newColor))
        {
            //we set this sprite equal to the sprite value of the color
            sprite.sprite = colorSpriteDict[newColor];
        }
    }
}
