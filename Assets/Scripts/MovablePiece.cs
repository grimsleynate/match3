﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//we make sure a GamePiece component is attached to this game object.
[RequireComponent(typeof (GamePiece))]

public class MovablePiece : MonoBehaviour
{
    private GamePiece piece; //a reference to this object's GamePiece component
    private IEnumerator moveCoroutine; //a reference to the move coroutine.
    private IEnumerator moveNoSwapCoroutine; 

    void Awake()
    {
        //we set our GamePiece component to the piece variable
        piece = GetComponent<GamePiece>();
    }
    // Start is called before the first frame update

    //Move function moves the piece
    public void Move(int newX, int newY, float time)
    {
        //if our stored coroutine is not null,
        if (moveCoroutine != null)
        {
            //stop it.
            StopCoroutine(moveCoroutine);
        }

        //we set our coroutine equal to MoveCoroutine
        moveCoroutine = MoveCoroutine(newX, newY, time);
        //we start the coroutine
        StartCoroutine(moveCoroutine);
    }

    public void MoveNoSwap(int newX, int newY, float time)
    {
        if (moveNoSwapCoroutine != null)
        {
            StopCoroutine(moveCoroutine);
        }

        moveNoSwapCoroutine = MoveNoSwapCoroutine(newX, newY, time);
        StartCoroutine(moveNoSwapCoroutine);
    }

    private IEnumerator MoveCoroutine(int newX, int newY, float time)
    {
        //we set the piece's x and y coordinates to their new coordinates.
        piece.X = newX;
        piece.Y = newY;

        //we set the starting position of our piece
        Vector3 startPos = transform.position;
        Vector3 endPos = piece.GridRef.GetWorldPosition(newX, newY);

        //we interpolate for time amount of seconds, updateing every frame.
        for (float t = 0; t <= 1 * time; t += Time.deltaTime)
        {
            //we set our current piece's position to our interpolation
            piece.transform.position = Vector3.Lerp(startPos, endPos, t / time);
            //wait for one frame
            yield return 0;
        }

        //we make sure the piece makes it to the end position.
        piece.transform.position = endPos;
    }

    private IEnumerator MoveNoSwapCoroutine(int newX, int newY, float time)
    {
        //we set the starting position of our piece
        Vector3 startPos = transform.position;
        Vector3 midPos = piece.GridRef.GetWorldPosition(newX, newY);

        //we interpolate for time amount of seconds, updating every frame.
        for (float t = 0; t <= 1 * time; t += Time.deltaTime)
        {
            //we set our current piece's position to our interpolation
            piece.transform.position = Vector3.Lerp(startPos, midPos, t / time);
            //wait for one frame
            yield return 0;
        }

        //we interpolate back to our original position
        for (float t = 0; t <= 1 * time; t += Time.deltaTime)
        {
            //we interpolate back to our starting position
            piece.transform.position = Vector3.Lerp(midPos, startPos, t / time);
            yield return 0;
        }

        //we make sure the piece makes it to the end position.
        piece.transform.position = startPos;
    }
}
