﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Grid : MonoBehaviour {

    public int xDim; //stores how many tiles wide the grid will be.
    public int yDim; //stores how many tiles tall the grid will be.
    public GameObject backgroundPrefab; //this stores our background for the tiles
    private Transform tf; //this stores this GameObject's transform component
    private Dictionary<PieceType, GameObject> piecePrefabDict; //we create a Dictionary that will store our piece types as a key and the associated game object as the value.
    private GamePiece[,] pieces; //we create a 2D array of game objects to store our pieces.
    private bool inverse = false;
    public PiecePrefab[] piecePrefabs; //an array of PiecePrefabs you can assign in the inspector.
    public float fillTime; //how quickly the board fills.
    private GamePiece pressedPiece; //the piece we press first.
    private GamePiece enteredPiece; //the piece we want to swap with.

    //this is an enumerator for our different piece types
    public enum PieceType
    {
        EMPTY, //An empty piece (for when a tile is cleared)
        NORMAL, //Normal piece type
        BUBBLE, //Bubble obstacle type
        COUNT, //A count of how many piece types there are.
    }

    //we create a struct to store our keys and values as one object
    //we make it serializable so it shows up in the inspector
    [System.Serializable]
    public struct PiecePrefab
    {
        public PieceType type;
        public GameObject prefab;
    }


    // Use this for initialization
    void Start() {
        //we store this object's transform component in its variable.
        tf = gameObject.GetComponent<Transform>();
        //we instantiate the array of pieces
        pieces = new GamePiece[xDim, yDim];
        //we set our Dictionary equal to a new Dictionary storing each pieceType and gameObject.
        piecePrefabDict = new Dictionary<PieceType, GameObject>();

        //we iterate through each item in the piecePrefab dictionary
        for (int i = 0; i < piecePrefabs.Length; i++)
        {
            //if the Dictionary doesn't already have that key
            if (!piecePrefabDict.ContainsKey(piecePrefabs[i].type))
            {
                //then we add this key value pair to the Dictionary
                piecePrefabDict.Add(piecePrefabs[i].type, piecePrefabs[i].prefab);
            }
        }

        //we run a for loop that will run while x is less than xDim
        for (int x = 0; x < xDim; x++)
        {
            //then for each x we run through every y in yDim
            for (int y = 0; y < yDim; y++)
            {
                //we create the background at the current x and y position of the grid, with no rotation (Quaternion.identity)
                GameObject background = Instantiate(backgroundPrefab, GetWorldPosition(x, y), Quaternion.identity);
                //we make this background a child of the grid object
                background.transform.parent = tf;
            }
        }

        //we loop through every piece on the grid, starting with the left column and moving right
        for (int x = 0; x < xDim; x++)
        {
            for (int y = 0; y < yDim; y++)
            {
                //we fill the grid in the beginning with empty pieces.
                SpawnNewPiece(x, y, PieceType.EMPTY);
            }
        }

        //we destroy the middle piece.
        Destroy(pieces[4, 4].gameObject);
        //we spawn a bubble piece instead.
        SpawnNewPiece(4, 4, PieceType.BUBBLE);

        StartCoroutine(Fill());
    }

    // Update is called once per frame
    void Update() {

    }

    //we return a Vector2 that stores the proper position for each tile to be centered in the game.
    public Vector2 GetWorldPosition(int x, int y)
    {
        return new Vector2(transform.position.x - xDim / 2.0f + x,
            transform.position.y + xDim / 2.0f - y);
    }

    //SpawnNewPiece returns a GamePiece
    public GamePiece SpawnNewPiece(int x, int y, PieceType type)
    {
        //we instantiate a new object of type 'type'.
        GameObject newPiece = Instantiate(piecePrefabDict[type], GetWorldPosition(x, y), Quaternion.identity);
        //we make this piece a child of the grid.
        newPiece.transform.parent = tf;
        //we store the new piece in our pieces array
        pieces[x, y] = newPiece.GetComponent<GamePiece>();
        //we call the Init function on the new piece to initiatilize it.
        pieces[x, y].Init(x, y, this, type);

        //we return the piece
        return pieces[x, y];
    }

    //returns true if the two pieces are adjacent
    public bool IsAdjacent(GamePiece piece1, GamePiece piece2)
    {
        //returns true if the x coordinates are the same and the y coordinates are one away from each other or the inverse.
        return (piece1.X == piece2.X && Mathf.Abs(piece1.Y - piece2.Y) == 1)
            || (piece1.Y == piece2.Y && Mathf.Abs(piece1.X - piece2.X) == 1); 
    }

    //swaps two piece if movable
    public void SwapPieces(GamePiece piece1, GamePiece piece2)
    {
        //if both pieces are movable,
        if (piece1.IsMovable() && piece2.IsMovable())
        {
            //swap them in our array.
            pieces[piece1.X, piece1.Y] = piece2;
            pieces[piece2.X, piece2.Y] = piece1;

            if (GetMatch(piece1, piece2.X, piece2.Y) != null || GetMatch(piece2, piece1.X, piece1.Y) != null)
            {
                //now we swap the pieces on our grid.
                //we store piece1's position in temporary variables so they don't get overridden.
                int piece1X = piece1.X;
                int piece1Y = piece1.Y;
                //we switch spots
                piece1.MovableComponent.Move(piece2.X, piece2.Y, fillTime);
                piece2.MovableComponent.Move(piece1X, piece1Y, fillTime);
            }
            else
            {
                piece1.MovableComponent.MoveNoSwap(piece2.X, piece2.Y, fillTime);
                piece2.MovableComponent.MoveNoSwap(piece1.X, piece1.Y, fillTime);
            }
        }
    }

    //called when we press on a piece
    public void PressPiece(GamePiece piece)
    {
        //we set the piece equal to our pressed piece variable.
        pressedPiece = piece;

    }

    //called when we press a second adjacent piece.0
    public void EnterPiece(GamePiece piece)
    {
        //we set our enteredPiece variable equal to piece.
        enteredPiece = piece;
    }

    public void ReleasePiece()
    {
        //if the pressed piece and the entered piece are adjacent,
        if (IsAdjacent(pressedPiece, enteredPiece))
        {
            //swap the tiles
            SwapPieces(pressedPiece, enteredPiece);
        }
    }

    //Fill will fill the entire grid with tiles.
    public IEnumerator Fill()
    {
        //while we were able to move a piece, do so.
        while(FillStep())
        {
            //this is for diagonal falling tiles. By switching inverse every fill,
            //we can pull from different columns to fill.
            inverse = !inverse;
            //we wait a designer-specified amount of time between running this coroutine.
            yield return new WaitForSeconds(fillTime);
        }
    }

    //StepFill will fill the grid after a match is made.
    //It returns true if any pieces were moved, and false if not.
    public bool FillStep()
    {
        //runs true if a piece is moved
        bool movedPiece = false;

        //we loop through the columns in reverse order
        for(int y=yDim-2; y >= 0; y--)
        {
            for (int loopX=0; loopX < xDim; loopX++)
            {
                //if we aren't inverted, x is equal to loopX,
                //which means we go from left to right.    
                int x = loopX;
                //if we are inverse,
                if(inverse)
                {
                    //traverse from right to left
                    x = xDim - 1 - loopX;
                }
                //we store the current game piece in a variable
                GamePiece piece = pieces[x, y];

                //if the piece is movable
                if (piece.IsMovable ())
                {
                    //we store the piece below this one
                    GamePiece pieceBelow = pieces[x, y + 1];

                    //if the piece below this one is empty,
                    if (pieceBelow.Type == PieceType.EMPTY)
                    {
                        //we destory the piece below.
                        Destroy(pieceBelow.gameObject);
                        //we move the tile down one.
                        piece.MovableComponent.Move(x, y + 1, fillTime);
                        //we store the piece in its proper place within the array
                        pieces[x, y + 1] = piece;
                        //we spawn a new empty piece at this piece's old location
                        SpawnNewPiece(x, y, PieceType.EMPTY);
                        //since we moved the piece, we change movedPiece to true.
                        movedPiece = true;
                    }
                    else
                    {
                        //diag -1 is to the left, diag 0 is in the middle, diag 1 is on the right
                        for (int diag = -1; diag <= 1; diag++)
                        {
                            //if we're trying to place a tile anywhere but right below,
                            if (diag != 0)
                            {
                                //the x coordinate of our diagonal piece.   
                                int diagX = x + diag;

                                if (inverse)
                                {
                                    diagX = x - diag;
                                }

                                //we check to see if the x coordinate is within the bounds of the board.
                                if (diagX >= 0 && diagX < xDim)
                                {
                                    //we get a reference to the diagonal piece
                                    GamePiece diagonalPiece = pieces[diagX, y + 1];

                                    //if that piece is empty
                                    if(diagonalPiece.Type == PieceType.EMPTY)
                                    {
                                        //we set a boolean to see if there is a piece above the empty piece
                                        bool hasPieceAbove = true;

                                        //we loop through the row above the empty space to see if there are pieces above
                                        for (int aboveY = y; aboveY >= 0; aboveY--)
                                        {
                                            //we store a reference to the piece above
                                            GamePiece pieceAbove = pieces[diagX, aboveY];

                                            //if the piece is movable
                                            if (pieceAbove.IsMovable())
                                            {
                                                //break out because the tile above can fill in the empty space
                                                break;
                                            }
                                            //if we encounter a piece that is not movable and not empty,
                                            else if(!pieceAbove.IsMovable() && pieceAbove.Type != PieceType.EMPTY)
                                            {
                                                //the piece above is immovable, so it can't be filled from above.
                                                hasPieceAbove = false;
                                                break;
                                            }
                                        }

                                        //if the empty tile can't be filled from above,
                                        if (!hasPieceAbove)
                                        {
                                            //we destroy the empty piece
                                            Destroy(diagonalPiece.gameObject);
                                            //we move the tile diagonally to fill the empty spot.
                                            piece.MovableComponent.Move(diagX, y + 1, fillTime);
                                            //we update our pieces array with the new layout
                                            pieces[diagX, y + 1] = piece;
                                            //we make a new empty piece where our piece was
                                            SpawnNewPiece(x, y, PieceType.EMPTY);
                                            break;
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }

        for (int x=0; x < xDim; x++)
        {
            //we store the piece below this one in a variable.
            GamePiece pieceBelow = pieces[x, 0];

            //if that piece is empty,
            if (pieceBelow.Type == PieceType.EMPTY)
            {
                //we destroy the empty piece
                Destroy(pieceBelow.gameObject);
                //we create a new piece
                GameObject newPiece = Instantiate(piecePrefabDict[PieceType.NORMAL], GetWorldPosition(x, -1), Quaternion.identity);
                //we make this object a child of the grid.
                newPiece.transform.parent = tf;

                //we update the position in our array
                pieces[x, 0] = newPiece.GetComponent<GamePiece>();
                //we initiate the new piece 
                pieces[x, 0].Init(x, -1, this, PieceType.NORMAL);
                //we move the piece on the grid to the top row.
                pieces[x, 0].MovableComponent.Move(x, 0, fillTime);
                //we randomly pick a color for the tile.
                pieces[x, 0].ColorComponent.SetColor((ColorPiece.ColorType)Random.Range(0, pieces[x, 0].ColorComponent.NumColors));
                //we change the movedPiece boolean to true.
                movedPiece = true;
            }
        }

        return movedPiece;
    }

    public List<GamePiece> GetMatch(GamePiece piece, int newX, int newY)
    {
        //if the piece is colored (which means it's movable
        if (piece.IsColored())
        {
            //store the color of the piece in a variable named color
            ColorPiece.ColorType color = piece.ColorComponent.Color;
            //we create new lists for all vertical tiles, horizontal tiles, and all tiles that match.
            List<GamePiece> horizontalPieces = new List<GamePiece>();
            List<GamePiece> verticalPieces = new List<GamePiece>();
            List<GamePiece> matchingPieces = new List<GamePiece>();

            //we iterate through first horizontally then vertically
            //first, we add this tile to the list.
            horizontalPieces.Add(piece);

            //we start a for loop that switches which we we go to count tiles.
            for (int dir = 0; dir <= 1; dir++)
            {
                //we now need to count all tiles. xOffset is how far we are away from the moved piece.
                for (int xOffset = 1; xOffset < xDim; xOffset++)
                {
                    int x;

                    if (dir == 0) //left
                    {
                        x = newX - xOffset;
                    }
                    else //right
                    {
                        x = newX + xOffset; 
                    }

                    //if the x coordinate goes outside the bounds of our grid,
                    if (x < 0 || x >= xDim)
                    {
                        break;
                    }
                    
                    //if this piece is colored and the color is the same as the original piece,
                    if (pieces[x, newY].IsColored() && pieces[x, newY].ColorComponent.Color == color)
                    {
                        //add it to the horizontal pieces list.
                        horizontalPieces.Add(pieces[x, newY]);
                    }
                    //if it's not, we stop traversing in this direction.
                    else
                    {
                        break;
                    }

                }
            }

            //we check if horizontal pieces has enough pieces to make a match
            if (horizontalPieces.Count >= 3 )
            {
                //if it has, add these pieces to the matching pieces list.
                for (int i = 0; i < horizontalPieces.Count; i++)
                {
                    matchingPieces.Add(horizontalPieces[i]);
                }
            }

            //if we have enough matching pieces in our matchingPieces list,
            if (matchingPieces.Count >= 3)
            {
                return matchingPieces;
            }

            //if we didn't make a horizontal match, try everything vertically
            verticalPieces.Add(piece);

            //we start a for loop that switches which we we go to count tiles.
            for (int dir = 0; dir <= 1; dir++)
            {
                //we now need to count all tiles. xOffset is how far we are away from the moved piece.
                for (int yOffset = 1; yOffset < xDim; yOffset++)
                {
                    int y;

                    if (dir == 0) //up
                    {
                        y = newX - yOffset;
                    }
                    else //down
                    {
                        y = newX + yOffset;
                    }

                    //if the x coordinate goes outside the bounds of our grid,  
                    if (y < 0 || y >= xDim)
                    {
                        break;
                    }

                    //if this piece is colored and the color is the same as the original piece,
                    if (pieces[y, newX].IsColored() && pieces[y, newX].ColorComponent.Color == color)
                    {
                        //add it to the vertical pieces list.
                        verticalPieces.Add(pieces[y, newX]);
                    }
                    //if it's not, we stop traversing in this direction.
                    else
                    {
                        break;
                    }

                }
            }

            //we check if horizontal pieces has enough pieces to make a match
            if (verticalPieces.Count >= 3)
            {
                //if it has, add these pieces to the matching pieces list.
                for (int i = 0; i < verticalPieces.Count; i++)
                {
                    matchingPieces.Add(verticalPieces[i]);
                }
            }

            //if we have enough matching pieces in our matchingPieces list,
            if (matchingPieces.Count >= 3)
            {
                return matchingPieces;
            }
        }

        return null;
    }
}
