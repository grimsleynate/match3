﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//This is the parent class for any game piece
public class GamePiece : MonoBehaviour
{
    private int x; //the game piece's x coordinate
    private int y; //the game piece's y coordinate
    private Grid.PieceType type; //the game piece's type
    private Grid grid; //we reference the grid in case we need to access information from it later
    private MovablePiece movableComponent; //this stores the MovablePiece component if this object has one.
    private ColorPiece colorComponent; //this stores the colorPiece component if this object has one.

    public int X
    {
        get { return x; }
        set
        {
            //if this object has a MovablePiece component
            if (IsMovable())
            {
                //set x.
                x = value;
            }
        }
    }

    public int Y
    {
        get { return y; }
        set
        {
            //if this object has a MovablePiece component
            if (IsMovable())
            {
                //set y.
                y = value;
            }
        }
    }

    public Grid.PieceType Type
    {
        //we provide a getter for this piece's type
        //we don't provide a setter because we don't want
        //other classes to be able to change the piece type.
        get { return type; }
    }

    public Grid GridRef
    {
        //we set a getter to return the grid if needed.
        get { return grid; }
    }

    public MovablePiece MovableComponent
    {
        get { return movableComponent; }
    }

    public ColorPiece ColorComponent
    {
        get { return colorComponent; }
    }

    //we create a method that initializes all the variables upon instantiation.
    //we use an underscore to differentiate between parameters passed into the function
    //and the parameters for our piece.
    public void Init(int _x, int _y, Grid _grid, Grid.PieceType _type)
    {
        x = _x;
        y = _y;
        grid = _grid;
        type = _type;
    }

    //we create a function to return whether this object is movable or not.
    public bool IsMovable()
    {
        return movableComponent != null;
    }

    public bool IsColored()
    {
        return colorComponent != null;
    }

    void Awake()
    {
        //if the object doesn't have a movable component script, this will return null.
        movableComponent = GetComponent<MovablePiece>();
        colorComponent = GetComponent<ColorPiece>();
    }

    private void OnMouseEnter()
    {
        //set this piece equal to enteredPiece on the grid singleton
        grid.EnterPiece(this);
    }

    private void OnMouseDown()
    {
        //set this piece equal to pressedPiece on the grid singleton
        grid.PressPiece(this);
    }

    private void OnMouseUp()
    {
        grid.ReleasePiece();
    }
}
